#include "stdafx.h"
#include <Windows.h>
#include <math.h>

double klasyczny(double x, double y)
{
	return sqrt(pow(x, 2) + pow(y, 2));
}

double nieklasyczny(double x, double y)
{
	if (x == 0)
		return y;
	else if (x >= y && x != 0)
		return sqrt(1 + pow(y / x, 2));
	else
		return sqrt(1 + pow(x / y, 2));
}

int main()
{
	double x, y, r;

	printf("Podaj x: ");
	scanf_s("%lf", &x);
	fseek(stdin, 0, SEEK_END);
	printf("Podaj y: ");
	scanf_s("%lf", &y);

	double w1 = klasyczny(x, y);
	double w2 = nieklasyczny(x, y);

	printf("wartosc klasycznego algorytmu %lf", w1);
	printf("\nwartosc nieklasycznego algorytmu %lf", w2);

	if (w1 == w2)
		printf("\nOba daly te same wyniki");
	else
		printf("\nRoznia sie wynikami o %ld", labs(w1 - w2));

	printf("\n");
	system("pause");
	return 0;
}

