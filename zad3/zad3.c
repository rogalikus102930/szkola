// zad3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>

char przesun(char znak, int przesuniecie)
{
	if (znak + przesuniecie >= 126)
	{
		return znak = 32 + (znak + przesuniecie - 126);//powrot na poczatek
	}
	else if (znak + przesuniecie <= 32)
	{
		return znak = 126 - (znak + przesuniecie + 32);
	}
	return znak + przesuniecie;
}

int main()
{
	char znaki[4];
	printf("Program szyfrujacy kodem cezara.\nPodaj 4 znakowa wiadomosc: ");
	znaki[0] = tolower(getchar());
	znaki[1] = tolower(getchar());
	znaki[2] = tolower(getchar());
	znaki[3] = tolower(getchar());
	printf("Podaj przesuniecie: ");
	int przesuniecie;
	scanf_s("%d", &przesuniecie);
	znaki[0] = przesun(znaki[0], przesuniecie);
	znaki[1] = przesun(znaki[1], przesuniecie);
	znaki[2] = przesun(znaki[2], przesuniecie);
	znaki[3] = przesun(znaki[3], przesuniecie);
	printf("Wiadomosc: %c%c%c%c\n", znaki[0], znaki[1], znaki[2], znaki[3]);

	system("pause");

    return 0;
}

