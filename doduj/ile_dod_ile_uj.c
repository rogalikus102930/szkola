#include<stdio.h>

int main()
{
	int n=1;                        //ilosc liczb
	int podane_liczby[5];           //podane przez użytkownika liczby a
	int ile_d, ile_u;               //ilosc liczb dodatnich i ujemnych
	int i;

	printf("Program oblicza ile jest dodatnich oraz ile ujemnych\n");
	printf("wsrod liczb podanych przez Ciebie.\n");
	printf("Autor: Dorota Dabrowska.\n\n");
	printf("Podaj ile liczb podasz (nie moze byc wiecej niz 5):\t");
	scanf_s("%d",&n);

	ile_d=0; 
	ile_u=0;

	for(i=0;i<n;i++)
	{
		printf("liczba %d:\t",i+1);
		scanf_s("%d",&podane_liczby[i]);
		if(podane_liczby[i]>0)
		{
			ile_d++;
		}
		else if(podane_liczby[i]<0)
		{
			ile_u++;
		}
	}
	printf("\n\nIlosc liczb dodatnich:\t%d.",ile_d);
	printf("\nIlosc liczb ujemnych.:\t%d.",ile_u);
	printf("\n\nKoniec programu.\n");
	getch();
	return 0;
}