// lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>

int main()
{
	double x;

	int poczatekPrzedzialu1 = 0;
	int koniecPrzedzialu1 = 22;
	int poczatekPrzedzialu2 = -33;
	int koniecPrzedzialu2 = 0;


	
	scanf_s("%lf", &x);

	if(x < poczatekPrzedzialu1 || x > koniecPrzedzialu1)
		printf("%lf nie nalezy do przedzialu 1", x);
	else
		printf("%lf nalezy do przedzialu 1", x);

	if (x < poczatekPrzedzialu2 || x > koniecPrzedzialu2)
		printf("%lf nie nalezy do przedzialu 2", x);
	else
		printf("%lf nalezy do przedzialu 2", x);
	
	system("pause");
    return 0;
}

