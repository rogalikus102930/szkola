// lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <math.h>

#define PI 3.141592

double dodaj(double x, double y)
{
	return x + y;
}

double odejmij(double x, double y)
{
	return x - y;
}

double pomnoz(double x, double y)
{
	return x * y;
}

double podziel(double x, double y)
{
	if (y == 0)
	{
		printf("\nNie mozna dzielic przez 0!");
		return 0;
	}
	return x / y;
}

double poteguj(double x, double wykladnik)
{
	return pow(x, wykladnik);
}

double pierwiastkuj(double x)
{
	return sqrt(x);
}

double degreesToRadians(double degrees)
{
	return degrees * PI / 180;
}

int main()
{
	int dzialanie;
	printf("Wybor funkcji:\nKlawisze [1-7] sluza do wyboru funkcji:\n1.dodawanie, 2.odejmowanie, 3.mnozenie, 4.dzielenie, 5.potegowanie, 6.pierwiastkowanie, 7.stopnie na radiany\n");
	scanf_s("%d", &dzialanie);
	double x, y;
	switch (dzialanie)
	{
	case 1:
		printf("Podaj pierwszy skladnik dodawania: ");
		scanf_s("%lf", &x);
		printf("Podaj drugi skladnik dodawania: ");
		scanf_s("%lf", &y);
		printf("Suma wynosi: %f", dodaj(x, y));
		break;
	case 2:
		printf("\nPodaj odjemna: ");
		scanf_s("%lf", &x);
		printf("\nPodaj odjemnik: ");
		scanf_s("%lf", &y);
		printf("\nRoznica wynosi: %f", odejmij(x, y));
		break;
	case 3:
		printf("Podaj skladnik: ");
		scanf_s("%lf", &x);
		printf("Podaj skladnik: ");
		scanf_s("%lf", &y);
		printf("Iloczyn wynosi: %f", pomnoz(x, y));
		break;
	case 4:
		printf("Podaj dzielna: ");
		scanf_s("%lf", &x);
		printf("Podaj dzielnik: ");
		scanf_s("%lf", &y);
		printf("Iloraz wynosi: %f", podziel(x, y));
		break;
	case 5:
		printf("Podaj podstawe:");
		scanf_s("%lf", &x);
		printf("Podaj potege:");
		scanf_s("%lf", &y);
		printf("Potega liczby %f wynosi: %f", x, poteguj(x, y));
		break;
	case 6:
		printf("Podaj liczbe pod pierwiastkiem:");
		scanf_s("%lf", &x);
		printf("Pierwiastek liczby %f wynosi: %f", x, pierwiastkuj(x));
		break;
	case 7:
		printf("Podaj stopnie:");
		scanf_s("%lf", &x);
		printf("%f stopni = %f radianow", x, degreesToRadians(x));
		break;
	default:
		printf("Wybor funkcji:\nKlawisze [1-6] sluza do wyboru funkcji:\n1.dodawanie, 2.odejmowanie, 3.mnozenie, 4.dzielenie, 5.potegowanie, 6.pierwiastkowanie\n");
		break;
	}

	printf("\n\n");
	main();//zapetlanie

	

	system("pause");
	return 0;
}

