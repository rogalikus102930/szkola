// InvertCase.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Windows.h>


int main()
{
	char znak;
	scanf_s("%c", &znak);

	if (iswdigit(znak))
	{
		printf("nie podano znaku z alfabetu");
		system("pause");
		return 0;
	}

	if (iswupper(znak))
	{
		znak = towlower(znak);
	}
	else
	{
		znak = towupper(znak);
	}
	printf("%c", znak);
	system("pause");
    return 0;
}

