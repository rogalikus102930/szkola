#include<stdio.h>

int do_drugiej(int x)
{
	return x*x;
}

double do_drugiej(double x)
{
	return x*x;
}

int main()
{
	int x=2;
	double y=2.5;
	printf("Program, ktory sie skompiluje w C++, a nieskompiluje w C.\n");
	printf("Autor: Dorota Dabrowska.\n\n");
	printf("%d do drugiej to %d\n",x,do_drugiej(x));
	printf("%g do drugiej to %g\n",y,do_drugiej(y));
	printf("\nKoniec programu.\n");
	return 0;
}