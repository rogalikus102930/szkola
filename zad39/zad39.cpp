
#include "stdafx.h"
#include <Windows.h>
#include <math.h>


int main()
{
	double x;

	printf("Podaj x: ");
	scanf_s("%lf", &x);

	double wartosc;

	if (x > 0)
		wartosc = pow(2, x);
	else
		wartosc = -1;
	printf("Wartosc funkcji: %lf", wartosc);

	system("pause");
	return 0;
}

